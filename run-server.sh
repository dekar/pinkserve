#!/bin/bash

if [[ "$OSTYPE" == "linux-gnueabihf" ]]; then
    eval $(luarocks path --bin)
    lua5.1 server/main.lua
elif [[ "$OSTYPE" == "cygwin" ]]; then
    lua server/main.lua
elif [[ "$OSTYPE" == "win32" ]]; then
    echo "Not supported!"
fi
