﻿local socket = require("socket")

function wait_for_client()
end

function hello_msg()
    print("aloha!")
end

function quit_msg()
    print("bye-bye!")
end

local messages = {
    ["hello"] = hello_msg,
    ["quit"] = quit_msg,
}

local c = assert(socket.connect("localhost", 10080))

assert(c:send("hello!\n"))
