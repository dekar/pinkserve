﻿local lovebird = require "lovebird"
local suit = require "suit"
local lume = require "lume"
local inspect = require "inspect"
local socket = require "socket"
local string = require "string"

local common = require "common"

local lg = love.graphics

local font = lg.newFont(12)
local ui_messages = {}
local ui_clear_delay = 2.0 -- in sec
local ui_messages_max = 40

local c = nil -- this is would be a socket object

function connect_to_server(name, port)
    local c = socket.tcp()
    c:settimeout(2)
    ret, err = c:connect(name, port)

    if not ret then
        ui_push_message("Error in connect_to_server() " .. err)
    end

    return c
end

function close_connection()
    if c then
        c:close()
        c = nil
    end
end

function love.load(arg)
    -- create client
    lovebird.update()
    lovebird.maxlines = 500

    love.window.setTitle("pinki client")

    timestamp = love.timer.getTime()
end

function ui_push_message(str, ...)
    ui_messages[#ui_messages + 1] = "> " .. string.format(str, ...)
end

function love.update(dt)
    lovebird.update()

    local time = love.timer.getTime()
    if (time - timestamp >= ui_clear_delay) then
        timestamp = love.timer.getTime()
        if #ui_messages >= ui_messages_max then
            table.remove(ui_messages)
        end
    end

    local port = 10080
    local _, h = lg.getDimensions()

    suit.layout:reset(20, h / 2)
    suit.layout:padding(3, 3)

    if suit.Button("Connect to localhost", suit.layout:row(128, 64)).hit then
        close_connection()
        connect_to_server("localhost", port)
    end

    if suit.Button("Connect to visualdoj.ru", suit.layout:row()).hit then
        close_connection()
        connect_to_server("visualdoj.ru", port)
    end

    if suit.Button("Send \"Quit\" to server", suit.layout:row()).hit and c then
        c:send("quit")
    end

    suit.layout:reset(20, h / 2)
    suit.layout:padding(3, 3)

    suit.layout:col(128, 64)
    if suit.Button("Send \"Hello\" to server", suit.layout:col()).hit and c then
        c:send("hello")
    end

    if c then -- trying read from server
        local line, err = c:receive()
        if not err then 
            ui_push_message("server >> " .. line)
        end
    end
end

function love.draw()
    local w, h = lg.getDimensions()
    local border = 40 --in pixels

    lg.setFont(font)
    for k, v in ipairs(ui_messages) do
        lg.printf(v, border, k * font:getHeight(), w, "left")
    end

    suit.draw()
end

function love.keypressed(key)
    if key == "escape" then
        love.event.quit()
    elseif key == "up" then
        --TODO Put custom send of data here
    end
end

function love.quit()
    close_connection()
end

