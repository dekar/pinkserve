local string = require "string"
local socket = require("socket")

local server = assert(socket.bind("*", 10080))
local ip, port = server:getsockname()
print("Please telnet to localhost on port " .. port)
print("After connecting, you have 10s to enter a line to be echoed")
local i = 1

function process_loop(c)
    c:settimeout(1)
    c:send(string.format("from server [%d] !\n", i))

    local quit = false

    while not quit do
        local line, err = c:receive()
        if not err then 
            if line == "quit\n" or line == "quit" then
                quit = true
            end

            print(line .. "\n")
            c:send(line .. "\n") 
        end
    end

    c:close()
end

while true do
    local client = server:accept()

    process_loop(client)

    i = i + 1
end
